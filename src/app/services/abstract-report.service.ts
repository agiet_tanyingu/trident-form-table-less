import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AbstractReportService {

  data: AbstractReportRow[] = [];
  private dataSource = new BehaviorSubject<AbstractReportRow[]>([]);
  currentData = this.dataSource.asObservable();

  constructor() { }

  getData() {
    const originalData = [
      {code: 'code1', name: 'name1', reportModule: 'rm1'},
      {code: 'code2', name: 'name2', reportModule: 'rm2'}
    ];
    this.data = originalData;
    this.dataSource.next(originalData);
  }

  putData(data: AbstractReportRow[]) {
    console.log('PUT: ');
    console.log(data);
    // getData();
  }

  postData(data: AbstractReportRow[]) {
    console.log('POST: ');
    console.log(data);
    // getData();
  }

  deleteData(code: string) {
    console.log('DELETE: ' + code);
    // getData();
  }
}

export interface AbstractReportRow {
  code: string;
  name: string;
  reportModule: string;
}