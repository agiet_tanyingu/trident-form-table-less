import { TestBed } from '@angular/core/testing';

import { AbstractReportService } from './abstract-report.service';

describe('AbstractFormService', () => {
  let service: AbstractReportService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AbstractReportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
