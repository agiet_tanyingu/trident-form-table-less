// import { Component } from '@angular/core';
// import { FormGroup, FormArray, FormControl } from '@angular/forms';
// import { Subject } from 'rxjs';

// @Component({
//   selector: 'app-root',
//   templateUrl: './app.component.html',
//   styleUrls: ['./app.component.css']
// })
// export class AppComponent {

//   activitiesForm = new FormGroup({
//     activities: new FormArray([]),
//     test: new FormControl('test')
//   });  activities1;
//   activities = this.activitiesForm.get('activities') as FormArray;
//   unsubscribe: Subject<any> = new Subject();
//   constructor() {}

//   ngOnInit(): void {
//     this.activities1 = ['test', 'test2', 'test3'];
//     this.generateActivities(this.activities1);
//   }

//   addNote(event: Event): void {
//     event.preventDefault();
//     this.activities.push(new FormControl(''));
//   }

//   removeNote(index: number): void {
//     this.activities.removeAt(index);
//   }

//   generateActivities(activities: any[]): void {
//     console.log(this.activitiesForm.controls['activities'] as FormArray);
//     this.clearFormArray(this.activitiesForm.controls['activities'] as FormArray);
//     for (const activity of activities) {
//       this.activities.push(new FormControl(''));
//     }
//   }

//   clearFormArray(formArray: FormArray): void {
//     while (formArray.length !== 0) {
//       formArray.removeAt(0);
//     }
//   }

//   onSubmit() {
//     console.log(this.activitiesForm.getRawValue());
//     console.log(this.activitiesForm);
//   }
// }

import { Component } from '@angular/core';
import { FormGroup, FormArray, FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { AbstractReportRow, AbstractReportService } from 'src/app/services/abstract-report.service';
import { takeUntil } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  activitiesForm = new FormGroup({
    activities: new FormArray([]),
  });
  activities1;
  activities = this.activitiesForm.get('activities') as FormArray;
  unsubscribe: Subject<null> = new Subject();

  headerArr: string[] = ['code', 'name', 'reportModule'];

  constructor(private abstractReportService: AbstractReportService, private snackBar: MatSnackBar) {}

  ngOnInit(): void {
    this.abstractReportService.getData();

    this.abstractReportService.currentData
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(data => {
        this.activities1 = data;
        this.generateForm(data);
      });

    // this.activities1 = [{name: 'nameA', value: 'valueA'}, {name: 'nameB', value: 'valueB'}, {name: 'nameC', value: 'valueC'}];
    // this.generateForm(this.activities1);
  }

  addNote(event: Event): void {
    event.preventDefault();
    this.activities.push(new FormGroup({ code: new FormControl(''), name: new FormControl(''), reportModule: new FormControl('')}));
  }

  removeNote(index: number): void {
    this.activities.removeAt(index);
  }

  generateForm(activities: AbstractReportRow[]): void {
    console.log(this.activitiesForm.controls['activities'] as FormArray);
    this.clearFormArray(this.activitiesForm.controls['activities'] as FormArray);
    for (const activity of activities) {
      this.activities.push(new FormGroup({
        name: new FormControl(activity.name),
        code: new FormControl(activity.code),
        reportModule: new FormControl(activity.reportModule)
      }));
    }
  }

  clearFormArray(formArray: FormArray): void {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  }

  onSubmit() {
    console.log(this.activitiesForm.getRawValue());
    console.log(this.activitiesForm);
    console.log(this.activities.controls);
    this.snackBar.open(
      'Abstract Report Form Submitted',
      null,
      {
        duration: 5000,
        horizontalPosition: 'end',
        verticalPosition: 'top'
      });
  }
}